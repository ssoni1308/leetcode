# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    """
    :type l1: ListNode
    :type l2: ListNode
    :rtype: ListNode
    """
    def addTwoNumbers(self, l1, l2):
        num1, num2 = "", ""
        while l1:
            num1 += str(l1.val)
            l1 = l1.next
        while l2:
            num2 += str(l2.val)
            l2 = l2.next      
        numRes = str(int(num1[::-1]) + int(num2[::-1]))[::-1]
        res = ListNode(0)
        cur = res
        for i in range(len(numRes)):
            cur.next = ListNode(numRes[i])
            cur = cur.next
        return res.next
    
            