class Solution(object):
    """
        :type points: List[List[int]]
        :type K: int
        :rtype: List[List[int]]
        """
    def kClosest(self, points, K):
        points.sort(key = lambda X: X[0]**2 + X[1]**2)
        return points[:K]