class Solution(object):
    """
    :type paragraph: str
    :type banned: List[str]
    :rtype: str
    """
    def mostCommonWord(self, paragraph, banned):
        words = re.findall(u'\w+', paragraph.lower())
        return collections.Counter(word for word in words if word not in banned).most_common(1)[0][0]

if __name__ == '__main__':
    s = Solution()
    print (s.mostCommonWord("Bob hit a ball, the hit BALL flew far after it was hit.", ["hit"]))