class Solution(object):
    """
    :type logs: List[str]
    :rtype: List[str]
    """
    def reorderLogFiles(self, logs):
        def func(log):
            head, tail = log.split(" ", 1)
            return (0, tail, head) if tail[0].isalpha() else (1,)

        return sorted(logs, key = func)