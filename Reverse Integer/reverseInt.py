class Solution(object):
    """
    :type x: int
    :rtype: int
    """
    def reverse(self, x):
        flag = False
        if x < 0:
            flag = True
            x=x*-1
        rev = 0    
        while(x > 0):    
            rem = x %10    
            rev = (rev *10) + rem    
            x = x //10  
        if flag:
            rev=rev*-1
        if rev >= 2**31 - 1 or rev <= -2**31:
            return 0
        return rev