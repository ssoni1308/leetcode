class Solution(object):
    """
    :type nums: List[int]
    :type target: int
    :rtype: List[int]
    """
    def twoSum(self, nums, target):
        hashTable = {}
        for i, num in enumerate(nums):
            if target - num in hashTable:
                return([hashTable[target-num], i])
                break
            hashTable[num] = i
        return ([])

if __name__ == '__main__':
    s = Solution()
    print (s.twoSum([2,7,11,15], 9))